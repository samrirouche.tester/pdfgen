import os
import errno

def createFile(file_path, text):
    filename = file_path
    if not os.path.exists(os.path.dirname(filename)):
        try:
            os.makedirs(os.path.dirname(filename))
            print("Directory: '% s' ------------------- created" % file_path)
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise

    with open(filename, "w") as f:
        f.write(text)
        f.close()

#get current path
Root = os.path.dirname(__file__)
conf_folder = "conf"
html_folder = "html"
conf_file = "gen.conf"
html_file = "gen.html"

Path = os.path.join(Root, conf_folder, conf_file)
createFile(Path, "$head:O=P:U=mm:F=A4:end; \n$page:end; \
\n$text:txt=text:x=0.0:y=50.0:color=[22, 23, 22]:font=16:gravity=C:end;")

Path = os.path.join(Root, html_folder, html_file)
createFile(Path, "<text>Hello World !</text>")

Path = os.path.join(Root, "res")
os.mkdir(Path)
print("Directory: '% s' ------------------- created" % Path)
