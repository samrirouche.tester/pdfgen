from fpdf import FPDF
from bs4 import BeautifulSoup

# the big process
class gen():
    def getParams(self, line):
        list = []
        args = line.split(':')
        for arg in args:
            if "=" in arg:
                param = arg.split("=")
                list.append(param[1].replace(";", ""))
        return list
        
    def process(self):
        text_file = open("html/gen.html", "r")
        file_data  = text_file.read()
        soup = BeautifulSoup(file_data, features="html.parser")

        conf_file = open("conf/gen.conf", "r")
        Lines  = conf_file.read().split(';')
        for line in Lines:
            if '#' in line:
                continue

            if '$head' in line:
                #get head config
                params = self.getParams(line)
                pdf = PDF(orientation=params[0], unit=params[1], format=params[2])
                pdf.set_margins(0, 0, 0)
                pdf.set_auto_page_break(False, 0)

            elif '$page' in line:
                #create page
                pdf.add_page()

            elif '$tatu' in line:
                # create tato
                params = self.getParams(line)
                image = soup.find(params[0]).string
                pdf.DrawTatu(image, params[1], float(params[2]), float(params[3]), float(params[4]), float(params[5]))
            
            elif '$shape' in line:
                #draw shape lines
                params = self.getParams(line)
                pdf.draw_lines(float(params[0]), params[1], float(params[2]))
            
            elif '$title' in line:
                params = self.getParams(line)
                title = soup.find(params[0]).string
                pdf.DrawTitle(title, float(params[1]), float(params[2]), params[3], int(params[4]))
            
            elif '$text' in line:
                params = self.getParams(line)
                message = soup.find(params[0]).string
                pdf.DrawText(message, float(params[1]), float(params[2]), params[3], int(params[4]), params[5])

            elif '$image' in line:
                params = self.getParams(line)
                image = soup.find(params[0]).string
                pdf.DrawImage(image, float(params[1]), float(params[2]), float(params[3]), float(params[4]))
        #draw pdf
        output = "output.pdf"
        pdf.output(output, 'F')

class PDF(FPDF):
    def draw_lines(self, padding, color, width):
        list_c = eval(color)
        self.set_line_width(width)
        self.set_fill_color(list_c[0], list_c[1], list_c[2])
        self.rect(padding, padding, (210.0 - 2 * padding),  (297.0 - 2 * padding), 'DF')
    
    def DrawTatu(self, image, position, x, y, w=20, h=20):
        if position == 'L':
            self.set_xy(x, y)
            self.image(image,  link='', type='', w=w, h=h)
        elif position == 'R':
            self.set_xy(x, y)
            self.image(image,  link='', type='', w=w, h=h)
        elif position == 'RL' or position == 'LR':
            self.set_xy(6.0,6.0)
            self.image(image,  link='', type='', w=20, h=20)
            self.set_xy(183.0,6.0)
            self.image(image,  link='', type='', w=20, h=20)
    
    def DrawTitle(self, title, x, y, color, fsize):
        list_c = eval(color)
        self.set_xy(x, y)
        self.set_font('Arial', 'B', fsize)
        self.set_text_color(list_c[0], list_c[1], list_c[2])
        self.cell(w=210.0, h=20.0, align='C', txt=title, border=0)
    
    def DrawText(self, file, x, y, color, fsize, gravity):
        #draw the text
        list_c = eval(color)
        self.set_xy(x, y)
        self.set_font('Arial', '', fsize)
        self.set_text_color(list_c[0], list_c[1], list_c[2])
        # insert the texts in pdf
        self.multi_cell(210.0, 8.0, txt = file, ln=2, align = gravity)

    def DrawImage(self, img_url, x, y, w, h):
        self.set_xy(x, y)
        self.image(img_url,  link='', type='', w=w, h=h)

#main process
gen = gen()
gen.process()