# Introduction to the project

This project allow us to generate PDFs from simple configuration file.
I had this idea and i wanted to share it with everybody in order to contribuate.
Feel free to edit the project and add some improuvments.

# Get Started

Clone the project below on your local directory.
1) Run the script setup.py
`python setup.py`
this will create folders and configurations

2) Edit and add your configuration to the file `/conf/gen.conf`
3) Add your ressources, texts ... etc to the html file in `/html/gen.html`
4) finaly, run the script gen.py
   ` python gen.py`

   this will create a PDF file called `output.pdf` at the root of project.

output :
![see the pdf](https://gitlab.com/samrirouche.tester/pdfgen/-/blob/main/output.pdf)
# Example of code 
gen.conf file :

```
$head:O=P:U=mm:F=A4:end;
$page:end;
$shape:padd=5.0:color=[255, 255, 255]:stroke=0.8:end;
$shape:padd=6.0:color=[255, 255, 255]:stroke=0.3:end;
$shape:padd=7.0:color=[255, 255, 255]:stroke=0.3:end;
$tatu:src=image1:pos=L:x=10.0:y=10.0:w=20:h=20:end;
$tatu:src=image1:pos=R:x=180.0:y=10.0:w=20:h=20:end;
$tatu:src=image1:pos=L:x=10.0:y=267.0:w=20:h=20:end;
$tatu:src=image1:pos=R:x=180.0:y=267.0:w=20:h=20:end;
$title:txt=title1:x=0.0:y=40.0:color=[12, 17, 20]:font=18:end;
$text:txt=infos:x=30.0:y=80.0:color=[22, 23, 22]:font=10:gravity=L:end;
```

gen.html file :

```
<text1>This is the text1</text1>
<text2>This is the text2</text2>
<title1>This is the title1 !</title1>
<text3>
here a long text
</text3>
<image1> res/img_1.jpg </image1>
<image2> res/img_2.jpg </image2>
```

see this line code : `$title:txt=title1:x=0.0:y=40.0:color=[12, 17, 20]:font=18:end;`
`title1` is get from html file (we get the content and not the tag)

# Explain the code

A simple code line starts with `$` followed by the `keyword`, the variables are declared like this :
`[name]:[value]` and to indicate the end of line i used the keyword `end;`

# Examples 
see the examples folder to understand how the code works, if you want to test the current example, 
you can just place each file in its directory path.

/conf/gen.conf
/html/gen.html

The output of the example is in the screens folder (see Capture2.png)
